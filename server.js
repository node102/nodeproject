const express = require('express')
const cors = require('cors')

const app = express()

app.use(cors('*'))
app.use(express.json())

const router = require('./Router/Person')
app.use('/person',router)



app.listen(3300,'0.0.0.0',()=>{

    console.log("Server Started on port 3300")
})