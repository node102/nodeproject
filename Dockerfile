FROM node
WORKDIR /src
COPY . .
RUN npm install
EXPOSE 3300
CMD node server.js