const express = require('express')
const db =require ('../db')
const utils = require('../utils')

const router =express.Router()

router.get('/',(req,res)=>{
const query = 'select * from person'
db.pool.execute(query,(error,result)=>{
    res.send(utils.CreateResult(error,result))
})
})
router.post('/',(req,res)=>{
    const { name,email,password,city }=req.body
    const query = `insert into person(name,email,password,city) values(?,?,?,?)`
    db.pool.execute(query,[name,email,password,city],(error,result)=>{
        res.send(utils.CreateResult(error,result))
    })
    })

module.exports=router